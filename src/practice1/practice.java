package practice1;


public class practice {
	
	static void largestSequence(int arr[])
	{
		int count=1,largestCount=0,endIndex=0;
		for(int i=1;i<arr.length;i++)
		{
			if(arr[i-1]<=arr[i])
			{
				count++;
			}
			else
			{
				
				if(largestCount<count)
				{
					endIndex = i-1;
					largestCount = count;
					count =1;
				}
			}
		}
		System.out.println("Largest Sequence : "+largestCount+"\nStarting Index : "+(endIndex-largestCount+1)+"  Ending Index : "+endIndex);
	}
	public static void main(String[] args) 
	{
		int arr[]= {1, 1, 2, 3, 3, 4, 5, 2, 4, 5, 6, 7, 8,9, 10, 11, 12, 6, -1, -2};
		largestSequence(arr);
	}

}
